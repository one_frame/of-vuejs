# oneframe vuejs-project-template

vue.js のプロジェクトテンプレートです。

<!-- TOC -->

- [oneframe vuejs-project-template](#oneframe-vuejs-project-template)
- [Buildに必要なコマンド](#buildに必要なコマンド)
- [Build Setup](#build-setup)
- [環境の概要](#環境の概要)
  - [目的](#目的)
  - [概要](#概要)
  - [主な構成](#主な構成)
- [開発コーディングガイド](#開発コーディングガイド)
  - [vueコーディングガイド](#vueコーディングガイド)
    - [その他表記まとめ](#その他表記まとめ)
  - [その他ガイド](#その他ガイド)
    - [SEO的指針](#seo的指針)
    - [jsを動的読み込みをしたい場合のimport記述](#jsを動的読み込みをしたい場合のimport記述)
  - [リント/コーディングガイド概要](#リントコーディングガイド概要)
    - [javascript](#javascript)
    - [css / scss](#css--scss)
      - [特に大事なCSSの記法を下記に抽出しました](#特に大事なcssの記法を下記に抽出しました)
    - [プルリクエスト時のgitCL(CircleCI)によるLintをやります](#プルリクエスト時のgitclcircleciによるlintをやります)
- [フォルダ / ファイル構成](#フォルダ--ファイル構成)
- [APIについて](#apiについて)
  - [open API Generator](#open-api-generator)
  - [apiManager.js](#apimanagerjs)
    - [local mockモード](#local-mockモード)
  - [todo:API認証](#todoapi認証)
- [Vuetifyについて](#vuetifyについて)
  - [CSSの上書き](#cssの上書き)
  - [icon(web-font)について](#iconweb-fontについて)
- [フォームバリデーション(Fomr Validation)について](#フォームバリデーションfomr-validationについて)
  - [多言語対応（VueI18n）の連携](#多言語対応vuei18nの連携)
  - [カスタムメッセージ（上書き）](#カスタムメッセージ上書き)
- [このテンプレートの組み込み済機能](#このテンプレートの組み込み済機能)
  - [Google Analytics (GA)](#google-analytics-ga)
  - [Sentry (cloud logging service)](#sentry-cloud-logging-service)
  - [組み込み済み Common Component](#組み込み済み-common-component)
- [Chrome拡張機能のインストール](#chrome拡張機能のインストール)
- [Visual Studio Code (VSC) の設定](#visual-studio-code-vsc-の設定)
  - [拡張機能](#拡張機能)
  - [VSCのプロジェクト毎の設定](#vscのプロジェクト毎の設定)

<!-- /TOC -->

# Buildに必要なコマンド
``` bash
npm/node/yarn
# 環境構築時のバージョンは
# npm = 6.4.1
# node = v10.15.2

# 環境に合わせインストールしてください
# Homebrew/nodebrewを使ってインストールするといいかも


# 本プロジェクトのパッケージマネージャーは npm ではなく yarn を利用します。
# npm で yarn をグローバルインストール
npm install -g yarn
```

# Build Setup

``` bash
# モジュールインストール
yarn install

# developmwnt mode
# serve with hot reload at localhost:8311 on memory
yarn start
yarn dev

# build for production with minification
yarn build
#distフォルダに出力されます

# build for production and view the bundle analyzer report
yarn build:report

# eslint for code(js,vue)
yarn lint:code
# eslint-config-prettier fix for code(js,vue)
yarn fix:code

# stylelint for css(scss)
yarn lint:css
# prettier-stylelint fix for css(scss)
yarn fix:css

# prettier fix for html
yarn fix:html
```

# 環境の概要

## 目的

1. チームで共通のルールを徹底
    * 開発環境やルールの統一
    * Lintなどを使い、人によってやり方（書き方）を変えない＝人力チェック漏れが少なくなる
1. できるだけ同じ事を複数回書かない
    * 複数箇所にあるコード/設定は不具合の元
1. なるべくあるものは利用し、独自部分/保守性に注力する
    * 一般的な機能は既存のライブラリがないかまず探してみる

→複数メンバーでの品質維持、短納期（効率化）を実現する



## 概要

vue.jsでの開発に必要な環境を網羅したつもりです。まだ未熟なところもあります。
基本的な思想は以下です。

* フロントは all vue.js で サーバーからAPIでjsonを取得するプロジェクトを想定しています。
* webpack4を利用し、画像含めすべてのファイルを一つのjsにまとめることができます。
  * productionは cssを切り出しています。
  * 速度の改善のためjsを分割できるようになっています。
* 通常の開発はファイルの更新を検知し自動で差分ビルドを行い、localhost:8xxxのサーバーが起動します。
  * 開発ビルド時は eslint で .js,.vue、style*lintで .scss のlintチェックを行います。
* babel7を利用して、ES6で記述してもレガシーブラウザに対応できます
  * browserslistで記述したブラウザに自動対応します。
* vue componentは、.vueファイルの中にはtemplate HTMLのみを記述し、`.vue + script.js + style.scss(scoped)`のセットでファイルを作成します。
* vuetifyという、UIライブラリが入っています。（詳細は後で）
* assets/scss配下の共通common.scssが自動に取り込まれます。
  * scssの$変数,mixinはどこでも使用できます。
* コミット前にlintを必ず行いチェックしクライアントでfixする方針です。
  * できる限り後述のエディタープラグインと連携し、同じルールで整形してからコミットしてください。
  * もしくは、`yarn lint:code`, `yarn lint:css`、`yarn fix:code`, `yarn fix:css`を実行することで、エディタープラグインがなくても、リント/整形可能です。
  * プルリク時にもgit clでlintチェックを行う予定です。
* 原則`jQuery`(依存ライブラリ)は禁止にします。


## 主な構成
* yarn-scripts
* webpack4
  * webpack-dev-server（開発用ローカルサーバー）
* babel7
  * @babel/polyfill
* lint/formatter
  * eslint（js/vue リント、整形）
  * stylelint, prettier-stylelint（css,scssリント, prettierで整形）
* vue
  * vue-router（router配下でルーティング、使用レイアウトの設定）
  * vue-router-layout（layouts配下でレイアウト定義）
  * vuex（storeフォルダ配下でデータの管理）
  * vue-meta（meta情報設定プラグイン）
  * vue-i18n（多言語切り替え）
  * vuetify (material design UI Component)
  * sentry (realtime logging cloud service)
  * vee-validate
* common
  * moment(date/time utility)
  * lodash(A modern JavaScript utility library delivering modularity)
  * vue-localstorage
  * Picturefill（responsive image polyfill.　srcset sizes on IE11）
* Visual Studio Code Plugin（強く推奨）
  * EditorConfig for VS Code
  * eslint
  * prettier
  * stylelint
  * Vetur
  * VueHelper

# 開発コーディングガイド

## vueコーディングガイド

Vue のスタイルガイドの優先度A〜優先度Dに従ってコーディングする
 [Vue スタイルガイド](https://jp.vuejs.org/v2/style-guide/index.html)

Vue公式のスタイルガイドで何通りか提案されているものがある中で、特に下記は必須とします。

1. 複数単語コンポーネント名
    * item のような1単語は禁止、TodoItem のような2単語をパスカルケース (PascalCase)で記載する
1. コンポーネントのデータ
    * コンポーネントの data は関数でなければなりません
1. キー付き v-for
    * 原則 v-for に対しては key を使用してください。
1. 単一ファイルコンポーネントのファイル名の形式
    * 上記、1.同様.vueファイル名はパスカルケース (PascalCase)で記述
1. 自己終了形式のコンポーネント
    * このスタイルガイドと違い、自己終了形式`<xxx/>`は一切使わないようにします。
1. ディレクティブの短縮記法は使用しない
    * ex. `:`(NG) >> `v-bind:`(OK) 、`@`(NG) >> `v-on:`(OK)
1. 単一インスタンスのコンポーネント名
    * ページごとに 1 回しか使われないものは、TheHeader のように The というプレフィックスで始める
1. プロパティ名の型式
    * 本プロジェクトは、templata内ではvueファイル名と同様パスカルケース (PascalCase)にします。
1. 複数の属性をもつ要素
    * 複数の属性をもつ要素は、1 行に 1 要素ずつ、複数の行にわたって書くべきです。
1. テンプレート内での単純な式
    * 原則、テンプレート内に計算式や複雑な式を書かないようにする。
    * どのように その値を算出するかではなく、 何が表示されるべきかを記述するように努力するべきです。

### その他表記まとめ
* プロパティ名(methods,props,computed,watchなどのjsの関数名、変数名)
  →キャメルケース(camelCase)
* vueコンポーネント名、ファイル名、teamplateのコンポーネントタグ名
  →パスカルケース (PascalCase)


## その他ガイド

### SEO的指針

* クリックして遷移する箇所は 原則`<router-link>`を推奨しますが、適用できない場合は div ではなく、aタグで書くようにし、遷移先のURLをhrefに記載してください。理由としてはSEO的に aタグを認識させるということと、ユーザーが右クリックができるようにするためです。
```html
//  NG
<div v-on:click="goDetail()">もっと見る</div>

//  hrefに画面遷移せずに他の処理を実行する方法
<a href="/deatil?category=2" v-on:click.prevent="goDetail()">もっと見る</a>
```

### jsを動的読み込みをしたい場合のimport記述

```js
//  router/index.js
import NextPage from '../pages/NextPage/NextPage'
↓
component: () => import(/* webpackChunkName: "nextpage" */ '@/pages/NextPage/NextPage')
```

## リント/コーディングガイド概要


### javascript

[eslint:standard](https://github.com/standard/standard) を必須にします。

* インテントはスペース２個
* 行末の ; (セミコロン)はなし
* string は "（ダブルクォーテーション） ではなく '（シングルクォーテーション） で囲う
* 関数の後ろの括弧()前はスペースを入れる

どうしても、lintから逃れたい場合は、ルール変更を検討するか、下記のようにコメントで回避できます。
```js
//  その行のみ許可
alert('foo'); // eslint-disable-line no-alert

//  次の1行のみ許可
// eslint-disable-next-line no-unused-vars
const a = 1;
```

### css / scss

* 基本ルール(lint)は、
[stylelint-config-standard](https://github.com/stylelint/stylelint-config-standard) を使用します。

* CSSプロパティの順番を規定します。規定は、
[stylelint-config-property-sort-order-smacss](https://github.com/cahamilton/stylelint-config-property-sort-order-smacss) を使用します。

* 命名ルール等は
[oneframeコーディングガイドライン](https://bitbucket.org/one_frame/coding-guideline/src/master/)
を参照してください。

* 原則、vue componet （パーツ）のcssは、`lang="scss" scoped`で記述し、そのコンポーネントをコピーしても使えるようにしてください。vuetifyの上書きcssは、`src/assets/scss/object/utility`に記載してください。
そうすることで結果的に`src/asset/scss`配下の common.scss はほぼ mixin, variable, utility だけになると想定されます。

#### 特に大事なCSSの記法を下記に抽出しました
- `.prefix-blockName_elementName.modifier`
  - Block には必ずプレフィックス（p,c,u）を付与し、必ず`.prefix-blockName` から始まるようにする
  - Block, Element 自体の単語は lowerCamelCase にする
  - Block と Element の文字区切りは`1文字`のアンダースコア
  - Modifier と State 記述（例：is-active）は `--` でつなげない。`.`でつなぐ

HTML
```html
<nav class="c-localMenu">
  <h2 class="c-localMenu_title">メニュー見出し</h2>
  <p class="c-localMenu_titleLabel">見出し説明</p>
  <ul class="c-localMenu_list">
    <li class="c-localMenu_list_item is-active"><a href="#">メニューA</a></li>
    <li class="c-localMenu_list_item"><a href="#">メニューB</a></li>
  </ul>
</nav>
```
CSS
```css
.c-localMenu { } /* Block */
  .c-localMenu_title { }  /* Element */
  .c-localMenu_titleLabel { }  /* Element */
  .c-localMenu_list { }  /* Element */
    .c-localMenu_list_item { }  /* Element */
      .c-localMenu_list_item.typeA { }  /* Modifierはマルチクラス */
      .c-localMenu_list_item.typeB { }  /* Modifierはマルチクラス */
      .c-localMenu_list_item.is-active { }  /* Stateもマルチクラス */

```
SCSS
```scss
.c-localMenu {
  &_title { }
  &_titleLabel { }
  &_list {
    &_item {
      &.typeA { }
      &.typeB { }
      &.is-active { }
    }
  }
}
```


### プルリクエスト時のgitCL(CircleCI)によるLintをやります
todo



# フォルダ / ファイル構成

```bash
├── .vscode  #vscode設定
├── build  #webpackのビルド設定
├── config  #developper,production設定
├── dist  #production buildの出力先
├── env  #環境設定
├── src  #全ソース @/ で参照可能
│   ├── api #open api generator から出力された .ts ファイル
│   ├── assets
│   │   ├── css
│   │   ├── fonts
│   │   ├── images
│   │   └── scss  #共通CSSのソース
│   │       ├── common.scss  #共通CSSとして取り込まれる
│   │       ├── foundation
│   │       │   ├── base  #resetや基本css
│   │       │   ├── function
│   │       │   ├── mixin  #レスポンシブルなど
│   │       │   └── variable  #colorやwidthなどの共通数値
│   │       ├── layout
│   │       ├── object
│   │       │   ├── component  #それ以上細かくしにくい部品
│   │       │   ├── project  #componentの配置が中心
│   │       │   └── utility  #単体で使える、animeや微調整
│   │       └── vendor #vender libs css
│   ├── components  #vueコンポーネント
│   │   ├── Top  #ページ依存のものはページ名配下に
│   │   │   └── SeachResult
│   │   └── common  #共通パーツ、ページにつき一回しか読み込まないものはTheをプレフィックスに
│   │       ├── TheFooter
│   │       └── TheHeader
│   ├── layouts  #ページレイアウトの定義
│   │   ├── LayoutDefault
│   │   └── LayoutHome
│   ├── pages  #ルーティングに対応するページのコンポーネント
│   │   ├── NotFound
│   │   ├── PageNext
│   │   └── PageTop
│   ├── router
│   ├── store  #状態管理（ここでAPIを使用します）
│   │   ├── common  #どのページでも使うもの
│   │   ├── components  #特定のコンポーネントで使うもの
│   │   └── pages  #主にページのごとで使うもの
│   └── utils  #汎用js、定数やAPI Manager
│   │   └── lang  #多言語対応文字列
│   └── test  #テスト系
│       └── mock  #APIのサンプルレスポンスをjsで定義
└── static  #外部ライブラリ
    ├── favicon
    └── lib
```

# APIについて

## open API Generator

apiのスキーマ`api.yaml` を`open api generator`の`axios-typescript`モードでendpointを生成します。（別リポジトリ）

その生成された`.ts`ファイルを`src/api`配下に置きます。（このファイルは編集してはいけません）

## apiManager.js

- `src/utils/apiManager.js`に使用するAPIの宣言を書きます。

- Vuexの`store`からAPIを呼び、commit(mutations call)します。
```
const actions = {
  async getData ({ commit }) {
    const response = await ApiManager.request('SchoolApi').getSchools()

    commit('setData', response.data)
  }
}

const mutations = {
  setData (state, data) {
    state.items = data
  }
}
```

### local mockモード

`src/test/mock`の配下にjsを配置すると以下で簡単なAPIのようにレスポンスを返すことができます。todo:後でproduction buildではでエラーが出るようにします。

```
const actions = {
  async getMockData ({ commit }) {
    let pageNo = 1
    const response = await ApiManager.requestMock('noticeList', pageNo)

    commit('setMockData', response.data)
  }
}
```
## todo:API認証

token,bare,local Strage


# Vuetifyについて

[Vuetify Document](https://vuetifyjs.com/en/getting-started/quick-start)(日本語版は説明がなぜか書いていないので英語版をおすすめします)

原則、ボタン、エフェクトなどのパーツとしてだけを利用する方針がよいと思います。
デザインによりますが、レイアウト系などCSS上書きの方がコストが掛かりそうな場合は、自前でコーディングを実装するようにしてください。

なお、carousel / slider は vuetifyにはいいものがないので、`vue-slick`を使ってください。

一応全てを読み込むとビルドサイズが大きくなるので vuetify-loader で必要なlibのみインストールしていますが、されないケースもあるようなので後で考えます。

## CSSの上書き

原則、汎用的に使うパーツの上書きは、`src/assets/scss/object/utility/_vuetifycustom.scss`にCSSを書きます。
```scss
// buttonの上書き例
.v-btn {
  border-radius: 8px;
  box-shadow: none !important;

  &__content {
    font-weight: bold;
  }
}
```


## icon(web-font)について

以下のiconを使用できます。

* [Material Design Icons](https://materialdesignicons.com/)
  * `mdi-` プレフィックスをつけて記述します。
  * ex. `<v-icon>mdi-account-circle</v-icon>`

尚、アプリケーションごとに設計して使う場合は以下のように定義できます。

```
Vue.use(Vuetify, {
  iconfont: 'mdi',
  icons: {
    'product': 'mdi-dropbox',
    'support': 'mdi-lifebuoy',
  }
})

...

<template>
  <v-icon>$vuetify.icons.product</v-icon>

  //  変数
  <v-icon v-text="platform"></v-icon>
</template>

```

# フォームバリデーション(Fomr Validation)について

今後、openAPIのスキーマから自動生成するようにする予定ですが、しばらくはフロントとバックエンドの双方でバリデーションを行います。

`vuetify + vee-validate`を使用します。

[vuetify validation Doc](https://vuetifyjs.com/en/components/forms#vee-validate)
[vee-validate Doc](https://baianat.github.io/vee-validate/)

```html
// 基本
<v-text-field
  placeholder="Email"
  required
  v-validate="'email|required|max:10'" // 組み込み済の条件
  data-vv-name="email"
  v-bind:error-messages="errors.collect('email')"
></v-text-field>
```

## 多言語対応（VueI18n）の連携
```js
// main.js

Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: navigator.language,
  fallbackLocale: 'ja',
  messages: {
    en: require('./utils/lang/en.json'),
    ja: require('./utils/lang/ja.json')
  }
})

Vue.use(VeeValidate,
  {
    i18n,
    i18nRootKey: 'validations',  // カスタムメッセージは/utils/lang/ja.jsonのvalidations配下に書く
    dictionary: {
      ja: require('vee-validate/dist/locale/ja'),  // 日本後デフォルト
      en: require('vee-validate/dist/locale/en')
    }
  }
)
```

## カスタムメッセージ（上書き）

```js
//  /utils/lang/ja.json
{
  "validations": {
    "attributes": {
      "email": "メールアドレス"  //  input name="email" の呼び名
    },
    "custom": {
      "email": {
        "email": "有効なメールアドレスではありません"
        "max": "なんか長すぎます"
      }
    }
  }
}
```

# このテンプレートの組み込み済機能

## Google Analytics (GA)

```js
//  main.js
Vue.use(VueAnalytics, {
  id: 'UA-138588890-1', //  ←ここを変えてください現在はこのテンプレート専用のGAです。
  router
})
```

## Sentry (cloud logging service)

本番ではSentryでKeyを取得してください。

```js
//  main.js
Sentry.init({
  dsn: 'https://86a24829e0a44b7ca2ac27580097990a@sentry.io/1446085',
  integrations: [
    new Integrations.Vue({
      Vue,
      attachProps: true
    })
  ]
})
```
基本Vue内のcatchは自動に取得しますが、promise内では自前でコード`Sentry.captureException(error)`を書く必要があります。



## 組み込み済み Common Component

[page/PageTest.vue](http://localhost:8311/test)にサンプルがあります

* loading animation
  ```js
  // vuejs(mixins)
  this.g_loadingInit()  //  screen & page reset
  this.g_loadingScreenPlus()  // screen loading counter + 1
  this.g_loadingScreenMinus()  // screen loading counter - 1
  this.g_loadingScreenReset()  // screen loading reset(counter=0)
  this.g_loadingPagePlus()
  this.g_loadingPageMinus()
  this.g_loadingPagenReset()

  // object loading
  import LoadingObject from '@/components/common/LoadingObject/LoadingObject'
  ...
  this.isLoadingObjectA = true
  ```

  ```js
  // vuex(store)からの呼び出し
  this.dispatch('loading/loadingInit')
  this.dispatch('loading/loadingScreenReset')
  this.dispatch('loading/loadingScreenPlus')
  this.dispatch('loading/loadingScreenMinus')
  this.dispatch('loading/loadingPageReset')
  this.dispatch('loading/loadingPagePlus')
  this.dispatch('loading/loadingPageMinus')
  
  ```
* alert
  ```
  g_addAlert(message, type, 自動消去秒数=0)
  ```
* dialog
  ```
  g_openDialog(title, message, Commitボタン名, キャンセルボタン名, モーダル, callback)
  ```
* error page
  ```
  this.$redirectErrorPage('message', statusCode)
  ```
* 多言語対応(vue-i18n)
  ```
  this.$i18n.locale = 'ja'
  ```


# Chrome拡張機能のインストール

[Vue.js devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)

Vueのdevelop modeのみ動作する便利ツールです。
Chrome develop tool に vue panel が表示されます。
表示されない場合は、Chromeの拡張機能設定で「ファイルの URL へのアクセスを許可する」を ON にしてください。


# Visual Studio Code (VSC) の設定

リアルタイムに`lint`、保存時に`auto formatter`することを強く推奨します。

## 拡張機能

  * EditorConfig for VS Code
    * ルートの.editorconfigを有効にする。
    * 今後社内外使用するプロジェクトが増えると思うので入れておく
  * eslint
    * ルートの.eslintrc.jsの設定を見て、jsコードがエディター上及び、下ウインドウの問題タブにリント結果がリアルタイムに表示されます。今回pretterとは連動しません。
  * stylelint
    * .stylelintrc.jsonの設定を見て、cssコードが、エディター上及び、下ウインドウの問題タブにリント結果がリアルタイムに表示されます。pretterとも連動しています。
  * prettier
    * 保存時に自動的にscssがフォーマットされます。stylelintの設定を見ています。
  * Vetur
  * VueHelper
    * .vueのコードカラーリングと、vue命令の補完ができます。

## VSCのプロジェクト毎の設定

ルートの.vscode/settings.jsonがあれば、上記プラグインが正常に動作しますので、各自自分の設定（があれば）とマージしてください。

```settings.json
{
  // デフォルトオフ
  "editor.formatOnSave": false,
  "css.validate": false,
  "scss.validate": false,
  "javascript.validate.enable": false,
  "javascript.format.enable": false,

  "vetur.format.defaultFormatter.html": "none",
  "vetur.validation.template": false,

  // js/vueはeslintで lint/fix
  "eslint.enable": true,
  "eslint.autoFixOnSave": true,
  "eslint.validate": [
    "javascript",
    {
      "language": "vue",
      "autoFix": true
    }
  ],

  // css/scssはprettierで lint/fix
  "prettier.stylelintIntegration": true,
  "[css]": {
    "editor.formatOnSave": true
  },
  "[scss]": {
    "editor.formatOnSave": true
  }

}
```
