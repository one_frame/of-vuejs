import TheHeader from '@/components/common/TheHeader/TheHeader'
import TheFooter from '@/components/common/TheFooter/TheFooter'
import LoadingPage from '@/components/common/LoadingPage/LoadingPage'
import LoadingObject from '@/components/common/LoadingObject/LoadingObject'

export default {
  components: {
    TheHeader,
    TheFooter,
    LoadingPage,
    LoadingObject
  },
  props: {
    source: String
  }
}
