import LoadingPage from '@/components/common/LoadingPage/LoadingPage'

export default {
  components: {
    LoadingPage
  },
  data () {
    return {
      drawer: null
    }
  }
}
