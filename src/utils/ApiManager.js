import { BookmarkApi, NoticeApi } from '../api'
import globalAxios from 'axios'
import redirectErrorPage from '../utils/redirectErrorPage'

const openApiOption = {
  //  CORSのためdevはproxyで取得
  basePath: process.env.API_ENDPOINT,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*'
  }
  // transformRequest: [function (data, headers) {
  //   // Do whatever you want to transform the data

  //   return data;
  // }],
  // transformResponse: [function (data) {
  //   // Do whatever you want to transform the data

  //   return data;
  // }]
}

const openApiClients = {
  'Bookmark': new BookmarkApi(openApiOption),
  'Notice': new NoticeApi(openApiOption)
}

export default {
  init: () => {
    let axios = globalAxios

    axios.interceptors.request.use(
      function (config) {
        console.log(config.url, config)
        return config
      },
      function (error) {
        console.log('request:', error)
        return Promise.reject(error)
      }
    )

    axios.interceptors.response.use(
      function (response) {
        console.log('response:', response.request.responseURL, response)
        return response
      },
      function (error) {
        console.log('response:', error)
        // 認証エラー時の処理
        if (error.response.status === 401) {
          redirectErrorPage('Error', error.response.status)
        // システムエラー時の処理
        } else if (error.response.status === 500) {
          redirectErrorPage('Error', error.response.status)
        } else {
          redirectErrorPage('Error', error.response.status)
        }
        return Promise.reject(error)
      }
    )
  },
  request: (types) => {
    return openApiClients[types]
  },
  requestMock: (method, page = null) => {
    let mockapi
    if (page !== null) {
      console.log('mock:req:', '../../test/mock/' + method + '_' + page)
      mockapi = require('../../test/mock/' + method + '_' + page).default.mock
    } else {
      console.log('mock:req:', '../../test/mock/' + method)
      mockapi = require('../../test/mock/' + method).default.mock
    }

    return new Promise(function (resolve, reject) {
      setTimeout(() => {
        console.log('mock:res:', mockapi)
        resolve(mockapi.data)
      }, 2000)
    })
  }
}
