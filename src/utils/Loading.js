import { mapActions } from 'vuex'

export default {
  created () {
    this.g_loadingInit()
    console.log('init')
  },
  methods: {
    ...mapActions({
      g_loadingInit: 'loading/loadingInit',
      g_loadingScreenPlus: 'loading/loadingScreenPlus',
      g_loadingScreenMinus: 'loading/loadingScreenMinus',
      g_loadingScreenReset: 'loading/loadingScreenReset',
      g_loadingPagePlus: 'loading/loadingPagePlus',
      g_loadingPageMinus: 'loading/loadingPageMinus',
      g_loadingPagenReset: 'loading/loadingPageReset'
    })
  }
}
