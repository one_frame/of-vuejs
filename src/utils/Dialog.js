export default {
  methods: {
    g_openDialog (title, message, buttonCommit = '', buttonCancel = '', isModal = false, fnEvent = null) {
      this.$store.dispatch('dialogscreen/openDialog', {
        title: title,
        message: message,
        buttonCommit: buttonCommit,
        buttonCancel: buttonCancel,
        modal: isModal,
        fnEvent: fnEvent
      })
    },
    g_closeDialog () {
      this.$store.dispatch('dialogscreen/closeDialog')
    },
    g_resetDialog () {
      this.$store.dispatch('dialogscreen/resetDialog')
    }
  }
}
