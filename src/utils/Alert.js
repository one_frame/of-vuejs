import { mapMutations } from 'vuex'

export default {
  methods: {
    ...mapMutations({
      g_resetAlert: 'alertscreen/resetAlert'
    }),
    g_addAlert (message, type, limitSecond) {
      this.$store.commit('alertscreen/addAlert', { message, type, limitSecond })
    }
  }
}
