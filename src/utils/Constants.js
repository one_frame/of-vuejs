export default {
  Meta: [
    {
      'http-equiv': 'X-UA-Compatible',
      content: 'IE=edge'
    },
    {
      name: 'viewport',
      content: 'width=device-width,initial-scale=1.0'
    }
  ],
  Label: {
    companyName: '会社名'
  },
  ApiStatus: {
    SUCCESS: '1000',
    ERROR: '1'
  }
}
