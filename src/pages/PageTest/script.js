import LoadingObject from '@/components/common/LoadingObject/LoadingObject'
import { mapState, mapActions } from 'vuex'
import bgImage1 from '@/assets/images/bgtest.png'

export default {
  name: 'PageTest',
  layout: 'LayoutNoSide',
  components: {
    LoadingObject
  },
  data () {
    return {
      isLoadingObjectA: false,
      btnLoader: null,
      btnLoading: false,
      styleBgImage: {}
    }
  },
  created () {
    // this.getInitData()
  },
  computed: {
    ...mapState({
    })
  },
  watch: {
    btnLoader () {
      const l = this.btnLoader
      this[l] = !this[l]

      setTimeout(() => (this[l] = false), 2000)

      this.btnLoader = null
    }
  },
  methods: {
    ...mapActions({
      getInitData: 'pagetest/getInitData'
    }),
    onTestScreenLoading () {
      this.g_loadingScreenPlus()
      setTimeout(() => {
        this.g_loadingScreenMinus()
      }, 2000)
    },
    onTestPageLoading () {
      this.g_loadingPagePlus()
      setTimeout(() => {
        this.g_loadingPageMinus()
      }, 2000)
    },
    onTestObjectLoading () {
      this.isLoadingObjectA = true
      setTimeout(() => {
        this.isLoadingObjectA = false
      }, 2000)
    },
    onTestAddAlert (message, type, limitSecond) {
      this.g_addAlert(message, type, limitSecond)
    },
    onTestDialog1 () {
      this.g_openDialog('ダイアログタイトル', 'デフォルトダイアログです。', 'OK', '', false, function (type) {
        console.log('dialog event', type)
      })
    },
    onTestDialog2 () {
      this.g_openDialog('ダイアログタイトル', 'modalダイアログです。（外側をクリックしても閉じません）', '理解しました', '閉じる', true, function (type) {
        console.log('dialog event', type)
      })
    },
    onTestErrorPage (message) {
      this.$redirectErrorPage(message, 345)
    },
    onTestChangeBg (n) {
      let path
      if (n === 1) {
        path = bgImage1
      } else {
        path = '/static/images/logo_icon_oneframe.png'
      }
      this.styleBgImage = {
        'backgroundImage': 'url(' + path + ')'
      }
    }
  },
  metaInfo () {
    this.$store.dispatch('meta/set', {
      title: 'page test',
      meta: [
        {
          name: 'description',
          content: '説明'
        },
        {
          name: 'keyword',
          content: 'keyword1,keyword2'
        }
      ]
    })
    return this.$store.getters['meta/get']
  }
}
