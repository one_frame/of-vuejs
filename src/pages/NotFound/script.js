export default {
  name: 'NotFound',
  layout: 'LayoutNoSide',
  components: {
  },
  metaInfo () {
    this.$store.dispatch('meta/set', {
      title: '404 not found'
    })
    return this.$store.getters['meta/get']
  }
}
