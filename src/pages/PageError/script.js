import { mapState } from 'vuex'

export default {
  name: 'PageError',
  layout: 'LayoutNoSide',
  components: {
  },
  props: {
    message: String,
    statusCode: Number
  },
  computed: {
    ...mapState({
    })
  },
  metaInfo () {
    this.$store.dispatch('meta/set', {
      title: 'Error'
    })
    return this.$store.getters['meta/get']
  }
}
