
export default {
  name: 'PageTOC',
  layout: 'LayoutNoSide',
  metaInfo () {
    this.$store.dispatch('meta/set', {
      title: 'NR:TOC',
      meta: [
        {
          name: 'description',
          content: 'リンク集'
        }
      ]
    })
    return this.$store.getters['meta/get']
  }
}
