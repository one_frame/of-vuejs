import { mapState } from 'vuex'
import TheBreadcrumbs from '@/components/common/TheBreadcrumbs/TheBreadcrumbs'
import DataTable from '@/components/common/DataTable/DataTable'

export default {
  name: 'PageNewsList',
  layout: 'LayoutDefault',
  components: {
    TheBreadcrumbs: TheBreadcrumbs,
    DataTable: DataTable
  },
  data () {
    return {
      breadcrumbs: [
        {
          text: 'Dashboard',
          disabled: false,
          href: 'breadcrumbs_dashboard'
        },
        {
          text: 'Link 1',
          disabled: false,
          href: 'breadcrumbs_link_1'
        },
        {
          text: 'Link 2',
          disabled: true,
          href: 'breadcrumbs_link_2'
        }
      ],
      companies: [
        'test'
      ]
    }
  },
  created () {},
  computed: {
    ...mapState({
      list: state => state.pagenewslist.list
    })
  },
  methods: {
    getNewsData (pageNo, sortField, sortType, freeword) {
      this.$store.dispatch('pagenewslist/getNewsData', {
        pageNo: pageNo,
        sortField: sortField,
        sortType: sortType,
        freeword: freeword
      })
    },
    onPagination (pagination) {
      console.log('onPagination', pagination)
      this.getNewsData(
        pagination.page,
        pagination.sortBy,
        pagination.descending,
        ''
      )
    }
  },
  metaInfo () {
    this.$store.dispatch('meta/set', {
      title: 'of-vue sample pagenext',
      meta: [
        {
          name: 'description',
          content: 'ニュース一覧'
        }
      ]
    })
    return this.$store.getters['meta/get']
  }
}
