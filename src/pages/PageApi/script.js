import { mapState, mapActions } from 'vuex'

export default {
  name: 'PageApi',
  layout: 'LayoutNoSide',
  components: {
  },
  data () {
    return {}
  },
  created () {
  },
  computed: {
    ...mapState({
      apires: state => state.pageapi.apires
    })
  },
  methods: {
    ...mapActions({
      getData: 'pageapi/getData'
    }),
    onTestApi () {
      this.getData()
    },
    onTestApiError () {
      this.getDataForError()
    }
  },
  metaInfo () {
    this.$store.dispatch('meta/set', {
      title: 'page API test',
      meta: [
        {
          name: 'description',
          content: '説明'
        },
        {
          name: 'keyword',
          content: 'keyword1,keyword2'
        }
      ]
    })
    return this.$store.getters['meta/get']
  }
}
