export default {
  name: 'DataTable',
  components: {},
  props: {
    loading: Boolean,
    headers: Array,
    items: Array,
    totalItems: Number,
    rowsPerPage: Number
  },
  data () {
    return {
      selected: [],
      innerPagination: {}
    }
  },
  watch: {
    innerPagination: {
      handler () {
        console.log('innerPagination', this.innerPagination)
        this.$emit('onPagination', this.innerPagination)
      },
      deep: true
    }
  },
  created () {},
  computed: {
    pages () {
      if (this.innerPagination.rowsPerPage == null ||
        this.totalItems == null
      ) return 0

      return Math.ceil(this.totalItems / this.innerPagination.rowsPerPage)
    }
  },
  methods: {
    onDeleteSelected () {
      this.g_openDialog('削除確認', '選択項目を削除します。よろしいですか？', '削除', 'キャンセル', false, () => {
        console.log('todo:delete ok', this.selected)
        this.selected.splice(0)
      })
    }
  }
}
