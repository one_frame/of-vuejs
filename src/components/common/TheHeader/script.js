import { mapState, mapActions } from 'vuex'

export default {
  name: 'TheHeader',
  components: {},
  props: {
    isSide: Boolean
  },
  data () {
    return {
      imgLogo: require('@/assets/images/bgtest.png')
    }
  },
  created () {},
  watch: {},
  computed: {
    ...mapState({
      sideBar: state => state.theside.sideBar
    })
  },
  methods: {
    ...mapActions({
      toggleSideBar: 'theside/toggleSideBar'
    })
  }
}
