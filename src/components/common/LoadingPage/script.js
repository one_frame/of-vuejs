import { mapState } from 'vuex'

export default {
  name: 'LoadingPage',
  computed: {
    ...mapState({
      isLoadingPage: state => state.loading.isLoadingPage
    })
  }
}
