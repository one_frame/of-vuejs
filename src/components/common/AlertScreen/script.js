import { mapState } from 'vuex'

export default {
  name: 'AlertScreen',
  computed: {
    ...mapState({
      items: state => state.alertscreen.items
    })
  }
}
