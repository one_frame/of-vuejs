import { mapState } from 'vuex'

export default {
  name: 'LoadingScreen',
  computed: {
    ...mapState({
      isLoadingScreen: state => state.loading.isLoadingScreen
    })
  }
}
