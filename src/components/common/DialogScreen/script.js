import { mapState } from 'vuex'

export default {
  name: 'DialogScreen',
  data: () => ({
    isDisplay: false,
    CloseType: ''
  }),
  computed: {
    ...mapState({
      dialog: state => state.dialogscreen.dialog,
      display: state => state.dialogscreen.dialog.display
    })
  },
  methods: {
    onClickCommit () {
      this.CloseType = 'commit'
      this.g_resetDialog()
    },
    onClickCancel () {
      this.CloseType = 'cancel'
      this.g_resetDialog()
    }
  },
  watch: {
    display (val) {
      if (this.isDisplay !== val) {
        this.isDisplay = val
      }
    },
    isDisplay (val) {
      if (!val) {
        if (this.display) {
          this.CloseType = 'outside'
          console.log('outside')
          this.g_resetDialog()
        }
        if (!val && this.dialog.fnEvent) {
          this.dialog.fnEvent(this.CloseType)
        }
        this.CloseType = ''
      }
    }
  }
}
