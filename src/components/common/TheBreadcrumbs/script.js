export default {
  name: 'TheBreadcrumbs',
  props: {
    breadcrumbs: Array
  }
}
