import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'
import { createRouterLayout } from 'vue-router-layout'

const RouterLayout = createRouterLayout(layout => {
  return import('@/layouts/' + layout + '/' + layout + '.vue')
})

Vue.use(Router)
Vue.use(Meta)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      component: RouterLayout,
      children: [
        {
          path: '',
          name: 'PageTOC',
          //  load sync
          component: require('@/pages/PageTOC/PageTOC').default
        },
        {
          path: '/newslist',
          name: 'PageNewsList',
          //  load sync
          component: require('@/pages/PageNewsList/PageNewsList').default
        },
        {
          path: '/test',
          name: 'PageTest',
          //  load dynamic
          component: () => import(/* webpackChunkName: "pagetest" */ '@/pages/PageTest/PageTest')
        },
        {
          path: '/data',
          name: 'PageApi',
          //  load dynamic
          component: () => import(/* webpackChunkName: "pageapi" */ '@/pages/PageApi/PageApi')
        },
        {
          path: '/error',
          name: 'PageError',
          props: true,
          //  load dynamic
          component: () => import(/* webpackChunkName: "pageerror" */ '@/pages/PageError/PageError')
        },
        //  リダイレクト
        {
          path: '/move',
          redirect: { name: 'PageTop' }
        },
        //  404
        {
          path: '*',
          name: 'NotFound',
          //  load dynamic
          component: () => import(/* webpackChunkName: "notfound" */ '@/pages/NotFound/NotFound')
        }
      ]
    }
  ]
})
