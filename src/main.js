import '@babel/polyfill'
import Vue from 'vue'
import App from '@/App'
import router from '@/router'
import store from '@/store'
//  logging いらない場合は削除
// import * as Sentry from '@sentry/browser'
// import * as Integrations from '@sentry/integrations'
import lodash from 'lodash'
import VueI18n from 'vue-i18n'
import VueLocalStorage from 'vue-localstorage'
import '@/assets/scss/common.scss'
import Vuetify from 'vuetify/lib'
// import colors from 'vuetify/es5/util/colors'
import 'vuetify/src/stylus/app.styl'
import VeeValidate from 'vee-validate'
import VueAnalytics from 'vue-analytics'
// global
import ApiManager from '@/utils/ApiManager'
import redirectErrorPage from '@/utils/redirectErrorPage'
//  mixins
import Alert from '@/utils/Alert'
import Loading from '@/utils/Loading'
import Dialog from '@/utils/Dialog'

Vue.config.productionTip = false

// --------------------------------------
//  logging いらない場合は削除
//  dsn は本テンプレート専用のID
// Sentry.init({
//   dsn: 'https://86a24829e0a44b7ca2ac27580097990a@sentry.io/1446085',
//   integrations: [
//     new Integrations.Vue({
//       Vue,
//       attachProps: true
//     })
//   ]
// })

// --------------------------------------
//  gaの設定
Vue.use(VueAnalytics, {
  //  oneframeの test account
  id: 'UA-138588890-1',
  router
})

// --------------------------------------
// lodash
// ex.: this.$_.orderBy(accounts, ['id', 'name'], ['asc', 'asc'])
Object.defineProperty(Vue.prototype, '$_', { value: lodash })

// --------------------------------------
// localstorage
// データ保存
// this.$localStorage.set(key,value)
// データ削除
// this.$localStorage.remove(key)
// データ取得
// this.$localStorage.get(key)
Vue.use(VueLocalStorage)

// --------------------------------------
Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: navigator.language,
  fallbackLocale: 'ja',
  messages: {
    ja: require('@/utils/lang/ja.json'),
    en: require('@/utils/lang/en.json')
  }
})

// --------------------------------------
Vue.use(VeeValidate,
  {
    i18n,
    i18nRootKey: 'validations',
    dictionary: {
      ja: require('vee-validate/dist/locale/ja'),
      en: require('vee-validate/dist/locale/en')
    }
  }
)

// --------------------------------------
Vue.use(Vuetify, {
  //  https://vuetifyjs.com/en/framework/breakpoints
  breakpoint: {
    thresholds: {
      xs: 0,
      sm: 0,
      md: 0,
      lg: 1200
    }
  },
  //  https://vuetifyjs.com/en/framework/colors
  theme: {
    primary: '#668899',
    secondary: '#424242',
    accent: '#FF4081',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  },
  //  https://vuetifyjs.com/en/framework/icons
  iconfont: 'mdi',
  icons: {
    'header-default': 'mdi mdi-circle-slice-6'
  }
})

// --------------------------------------
// redirectErrorPage
// ex.: this.$redirectErrorPage('message', statusCode)
Object.defineProperty(Vue.prototype, '$redirectErrorPage', { value: redirectErrorPage })

window.ApiManager = ApiManager
ApiManager.init()

// Constants['_store'] = store
// Constants['accountCheck'] = (callback) => {
//   return Constants._store.dispatch('account/check', callback)
// }
// Constants['getUserId'] = () => {
//   return Constants._store.getters['account/getUserId']
// }
// Constants['getImagePath'] = () => {
//   return Constants._store.getters['account/getImagePath']
// }
// window.Constants = Constants

const app = new Vue({
  el: '#app',
  i18n: i18n,
  router,
  mixins: [
    Loading,
    Alert,
    Dialog
  ],
  components: { App },
  template: '<App/>',
  store
})
app.$mount('#app')
