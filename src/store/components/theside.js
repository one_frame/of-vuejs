const state = {
  sideBar: {
    model: true,
    mini: false
  }
}

const actions = {
  toggleSideBar ({ commit }) {
    commit('toggleSideBar')
  }
}

const mutations = {
  toggleSideBar (state) {
    state.sideBar.mini = !state.sideBar.mini
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
