const state = {
  items: [{
    id: 0,
    type: 'success',
    message: '',
    display: false,
    limitSecond: 0
  }],
  keyCounter: 0,
  isFirst: true
}

const mutations = {
  addAlert (state, { message, type, limitSecond }) {
    let obj = {}
    if (state.isFirst) {
      obj = state.items[0]
    }
    obj.id = state.keyCounter
    state.keyCounter++
    obj.type = type
    obj.message = message
    obj.display = true
    obj.limitSecond = limitSecond
    if (!state.isFirst) {
      state.items.push(obj)
    }
    state.isFirst = false

    if (obj.limitSecond > 0) {
      setTimeout(() => {
        obj.display = false
      }, limitSecond * 1000)
    }
  },
  resetAlert (state, { message, type, limitSecond }) {
    state.items.length = 1
    state.isFirst = true
    mutations.addAlert(state, { message, type, limitSecond })
  }
}

export default {
  namespaced: true,
  state,
  mutations
}
