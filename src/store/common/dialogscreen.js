const state = {
  dialog: {
    display: false,
    title: '',
    message: '',
    modal: false,
    buttonCommit: 'OK',
    buttonCancel: '',
    fnEvent: null
  }
}

const actions = {
  openDialog ({ commit }, params) {
    commit('openDialog', params)
  },
  closeDialog ({ commit }) {
    commit('closeDialog')
  },
  resetDialog ({ commit }) {
    commit('resetDialog')
  }
}

const mutations = {
  openDialog (state, params) {
    state.dialog.fnEvent = params.fnEvent

    state.dialog.title = params.title
    state.dialog.message = params.message
    state.dialog.modal = params.modal
    if (!params.buttonCommit && !params.buttonCancel) {
      state.dialog.buttonCommit = 'OK'
      state.dialog.buttonCancel = ''
    } else {
      state.dialog.buttonCommit = params.buttonCommit
      state.dialog.buttonCancel = params.buttonCancel
    }
    state.dialog.fnEvent = params.fnEvent
    state.dialog.display = true
  },
  closeDialog (state) {
    state.dialog.display = false
    state.dialog.message = ''
  },
  resetDialog (state) {
    state.dialog.display = false
    state.dialog.message = ''
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
