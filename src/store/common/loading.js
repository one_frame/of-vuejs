const state = {
  isLoading: false,
  isLoadingScreen: false,
  loadingScreenCounter: 0,
  isLoadingPage: false,
  loadingPageCounter: 0
}
const actions = {
  loadingInit ({ commit }) {
    commit('init')
  },
  loadingScreenPlus ({ commit }) {
    commit('countUpScreen')
  },
  loadingScreenMinus ({ commit }) {
    commit('countDownScreen')
  },
  loadingScreenReset ({ commit }) {
    commit('resetScreen')
  },
  loadingPagePlus ({ commit }) {
    commit('countUpPage')
  },
  loadingPageMinus ({ commit }) {
    commit('countDownPage')
  },
  loadingPageReset ({ commit }) {
    commit('resetPage')
  }
}

const mutations = {
  init (state) {
    state.loadingScreenCounter = 0
    state.isLoadingScreen = false
    state.loadingPageCounter = 0
    state.isLoadingPage = false
    state.isLoading = false
  },
  countUpScreen (state) {
    state.loadingScreenCounter++
    state.isLoadingScreen = true
    state.isLoading = state.isLoadingScreen || state.isLoadingPage
  },
  countDownScreen (state) {
    if (state.loadingScreenCounter > 0) {
      state.loadingScreenCounter--
    }
    if (state.loadingScreenCounter === 0) {
      state.isLoadingScreen = false
    } else {
      state.isLoadingScreen = true
    }
    state.isLoading = state.isLoadingScreen || state.isLoadingPage
  },
  resetScreen (state) {
    state.loadingScreenCounter = 0
    state.isLoadingScreen = false
    state.isLoading = state.isLoadingScreen || state.isLoadingPage
  },
  countUpPage (state) {
    state.loadingPageCounter++
    state.isLoadingPage = true
    state.isLoading = state.isLoadingScreen || state.isLoadingPage
  },
  countDownPage (state) {
    if (state.loadingPageCounter > 0) {
      state.loadingPageCounter--
    }
    if (state.loadingPageCounter === 0) {
      state.isLoadingPage = false
    } else {
      state.isLoadingPage = true
    }
    state.isLoading = state.isLoadingScreen || state.isLoadingPage
  },
  resetPage (state) {
    state.loadingPageCounter = 0
    state.isLoadingPage = false
    state.isLoading = state.isLoadingScreen || state.isLoadingPage
  }
}
export default {
  namespaced: true,
  state,
  actions,
  mutations
}
