import Vue from 'vue'
import Vuex from 'vuex'
import alertscreen from './common/alertscreen'
import loading from './common/loading'
import dialogscreen from './common/dialogscreen'
import meta from './common/meta'
import theside from './components/theside'
import account from './common/account'
import pageapi from './pages/pageapi'
import pagenewslist from './pages/pagenewslist'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    alertscreen,
    loading,
    dialogscreen,
    meta,
    theside,
    account,
    pageapi,
    pagenewslist
  }
})
