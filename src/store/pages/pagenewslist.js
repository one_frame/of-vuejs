import ApiManager from '../../utils/ApiManager'

const state = {
  initLoaded: false,
  list: {
    loading: false,
    rowsPerPage: 4,
    totalItems: 0,
    // pagination: {
    //   descending: false,
    //   page: 1,
    //   rowsPerPage: 10,
    //   sortBy: null,
    // },
    headers: [
      { text: 'タイトル', align: 'left', value: 'title' },
      { text: '開始期間', align: 'left', value: 'display_from' },
      { text: '終了期間', align: 'left', value: 'display_to' }
    ],
    items: []
  }
}

const actions = {
  /**
   * お知らせデータ取得
   */
  async getNewsData ({ state, commit }, { pageNo, sortField, sortType, freeword }) {
    commit('setListLoading', true)
    const response = await ApiManager.request('Notice').noticePublic(
      pageNo,
      state.list.rowsPerPage,
      sortField,
      freeword
    )

    setTimeout(() => {
      commit('setList', response.data)
    }, 2000)
  }

}

const mutations = {
  init (state, data) {
    // 初期ローディングの終了
    state.initLoaded = true
  },
  setListLoading (state, data) {
    // Listローディング状態
    state.list.loading = data
    this.dispatch('loading/loadingPagePlus')
  },
  setList (state, data) {
    state.list.items = data.data
    state.list.totalItems = 8 //  state.list.items.length
    state.list.loading = false
    this.dispatch('loading/loadingPageMinus')
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
