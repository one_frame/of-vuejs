import ApiManager from '../../utils/ApiManager'

const state = {
  apires: []
}

const actions = {
  /**
   * データ読出し
   */
  async getData ({ commit }) {
    const response = await ApiManager.request('Bookmark').bookmarkList(1)

    commit('setData', response.data)
  },
  async getDataForError ({ commit }) {
    const response = await ApiManager.request('Bookmark').bookmarkList(1, 1, 1, 1, 1)
    commit('setData', response.data)
  }
}

const mutations = {
  setData (state, data) {
    state.apires = data
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
