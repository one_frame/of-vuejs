'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // API_ENDPOINT: '"http://localhost:8410/data"'
  API_ENDPOINT: '"http://34.85.50.218:8184"'
})
