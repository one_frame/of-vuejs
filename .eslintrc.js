module.exports = {
  root: true,
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true
  },
  extends: [
    'standard',
    'plugin:vue/strongly-recommended'
  ],
  plugins: [
    'vue'
  ],
  rules: {
    'no-console': 0,
    // 'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'generator-star-spacing': 'off',
    'vue/require-default-prop': 0,
    'vue/no-unused-components': 0,
    'vue/html-self-closing': ['error', {
      'html': {
        "void": "never",
        "normal": "never",
        "component": "never"
      },
    }]
    }
}
