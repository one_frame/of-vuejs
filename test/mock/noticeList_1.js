export default {
  mock: {
    config: {},
    headers: {},
    status: 200,
    statusText: '',
    data: {
      code: 200200,
      meta: {
        total: 7
      },
      data: [
        {
          id: 1,
          title: '1本環境は開発環境です',
          display_from: '2019-02-21T18:14:20+09:00',
          display_to: '2500-12-31T23:59:59+09:00',
          body: '本環境は開発環境ですのでご注意ください。'
        },
        {
          id: 2,
          title: '2アップデート後のJavaScriptキャッシュ更新について',
          display_from: '2019-01-23T12:29:54+09:00',
          display_to: '2500-12-31T23:59:59+09:00',
          body: 'アップデート後にJavaScriptのキャッシュが残っていて、画面が動かない場合があります。<br> その場合は何度かF5を押して頂くか、Chromeの場合は以下の手順でキャッシュを削除出来ます。<br> <a href="http://cms.aiosl.jp/manual/kowaza/1077/" target="_blank" style="text-decoration: underline;">スーパーリロード手順（外部サイト）</a>'
        },
        {
          id: 3,
          title: '3全体周知',
          display_from: '2019-01-11T15:13:04+09:00',
          display_to: '2500-12-31T23:59:59+09:00',
          body: 'テスト用メッセージ'
        },
        {
          id: 4,
          title: '4記念すべき最初のお知らせ',
          display_from: '2019-01-10T07:37:18+09:00',
          display_to: '2500-12-31T23:59:59+09:00',
          body: 'ああああああああああああああ <a href="http://google.com">ぐーぐる</a>'
        },
        {
          id: 5,
          title: '5本環境は開発環境です',
          display_from: '2019-02-21T18:14:20+09:00',
          display_to: '2500-12-31T23:59:59+09:00',
          body: '本環境は開発環境ですのでご注意ください。'
        }
      ]
    }
  }
}
