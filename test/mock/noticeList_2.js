export default {
  mock: {
    config: {},
    headers: {},
    status: 200,
    statusText: '',
    data: {
      code: 200200,
      meta: {
        total: 7
      },
      data: [
        {
          id: 6,
          title: '6本環境は開発環境です',
          display_from: '2019-02-21T18:14:20+09:00',
          display_to: '2500-12-31T23:59:59+09:00',
          body: '本環境は開発環境ですのでご注意ください。'
        },
        {
          id: 7,
          title: '7本環境は開発環境です',
          display_from: '2019-02-21T18:14:20+09:00',
          display_to: '2500-12-31T23:59:59+09:00',
          body: '本環境は開発環境ですのでご注意ください。'
        }
      ]
    }
  }
}
